#!/bin/sh
LOGFILE=jenkins-update-mirror.log
# enable debugging
set -x
# create local mirror for internal use

#SRCURL=rsync://rsync.osuosl.org/jenkins/updates/
#SRCURL=http://updates.jenkins-ci.org/stable/
#rsync -avz --include=stable* $SRCURL .
if [ -z "$1" ]; then
	echo "Missing LTS version"
	curl http://updates.jenkins-ci.org/ | grep stable
	exit 1
fi
LTS=$1

SRCURL=http://updates.jenkins-ci.org/stable-$LTS/
TARGETDIR=$LTS
REJECTLIST='*aix*','*darwin*','*ppc*','*arm*','*s390*','index.html*'
REJECT="--reject $REJECTLIST"

mkdir -p $TARGETDIR
cd $TARGETDIR || (echo "$TARGETDIR does not exist. Exit 1"; exit 1)
# --no-clobber not combinable with timestamping
WGETOPTS="--no-host-directories --recursive --continue --no-parent --progress=dot:mega --wait 1"
time wget -o $LOGFILE $WGETOPTS $REJECT $SRCURL
echo Summarize downloads: saved and error
grep -i saved $LOGFILE
du -sh *

# create local-update-center.json for local server from mirrored update-center.json
# niet nodig voor handmatige upgrades
